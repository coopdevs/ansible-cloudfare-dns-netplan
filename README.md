Netplan Cloudfare DNS
=========

Add Cloudfare's DNS nameservers via netplan to Ubuntu Server conf.

Requirements
------------

### Configure python environment

In order to assure that the playbooks contained here are run always with the same ansible version and linted the same way, we are using [`pyenv>

Follow [Installing and using pyenv](https://github.com/coopdevs/handbook/wiki/Installing-and-using-pyenv), or, in short:

```sh
pyenv install 3.8.12
pyenv virtualenv 3.8.12 dns-netplan
```

### Configure ansible environment

You will need Ansible on your machine to run the playbooks, follow the steps below to install it.

```sh
pyenv exec pip install -r requirements.txt
ansible-galaxy install -r requirements.yml -f
```

### Install pre-commit hooks

We use [pre-commit framework](https://pre-commit.com/) to assure quality code.

```sh
pre-commit install
```

Role Variables
--------------

None.

Example Playbook
----------------

 [playbook](/playbooks/change-dns.yml)

License
-------

Fair

Author Information
------------------

